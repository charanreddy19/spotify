import { Component, OnInit,Inject, ViewChild, ElementRef,Input} from '@angular/core';
import {SongsService} from '../songs.service'
import {ActivatedRoute, ParamMap} from '@angular/router'

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css']
})
export class SongComponent implements OnInit {
  src:any
  image:any
  name:any
  id:any=1
  play:boolean=false
  autoplay:any=false
  songs:any
  constructor(private song:SongsService,private route:ActivatedRoute) { 
    this.song.song.subscribe((value:any)=>{
      this.songs=value
      this.src=this.songs.url
      this.image=this.songs['image_url']
      this.name=this.songs.name
      this.autoplay=true
    })
  }

  ngOnInit(): void {
    console.log(this.songs)
    this.song.last().then((data:any)=>{
      this.songs=data
      this.src=this.songs.url
      this.image=this.songs['image_url']
      this.name=this.songs.name
    })
  }

  ngOnChanges(changes:any) {
    this.ngOnInit()
  }
}