import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap } from '@angular/router';
import { SongsService } from '../songs.service';

@Component({
  selector: 'app-recentall',
  templateUrl: './recentall.component.html',
  styleUrls: ['./recentall.component.css']
})
export class RecentallComponent implements OnInit {
  songs:any=[]
  obj:any
  constructor(private song:SongsService,private route:ActivatedRoute) {
    this.song.song.subscribe((value:any)=>{
      this.obj=value
    })
   }

  ngOnInit(): void {
    this.song.recent().then((data:any)=>{
      this.songs=data
   })
  }
  play(song:any) {
    this.song.song.next(song)
  }
}
