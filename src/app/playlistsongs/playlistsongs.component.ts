import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap } from '@angular/router';
import { SongsService } from '../songs.service';

@Component({
  selector: 'app-playlistsongs',
  templateUrl: './playlistsongs.component.html',
  styleUrls: ['./playlistsongs.component.css']
})
export class PlaylistsongsComponent implements OnInit {
  songs:any=[]
  obj:any
  allSongs:any=[]
  playlistName:any
  constructor(private song:SongsService,private route:ActivatedRoute,private router:ActivatedRoute) {
    this.song.song.subscribe((value:any)=>{
      this.obj=value
    })
    this.song.playlistName.subscribe((value:any)=>{
      this.playlistName=value
    })
   }

  ngOnInit(): void {
    this.router.paramMap.subscribe((params:ParamMap)=>{
      this.playlistName=params.get('name')
    })
    this.song.playlistSongs(this.playlistName).then((data:any)=>{
      this.songs=data
    })
    this.song.toAddPlaylistSongs(this.playlistName).then((data:any)=>{
      this.allSongs=data
    })
  }

  play(song:any) {
    this.song.song.next(song)
  }

  add(song:any) {
      let index=console.log(this.allSongs.indexOf(song))
      this.allSongs.splice(index,1)
      this.songs.push(song)
      this.song.addSongToPlaylist(song.id,this.playlistName).then((data:any)=>{
         console.log(data)
       })
  }

  ngOnChanges() {
    this.ngOnInit()
  }

}
