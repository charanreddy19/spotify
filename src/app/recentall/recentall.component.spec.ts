import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentallComponent } from './recentall.component';

describe('RecentallComponent', () => {
  let component: RecentallComponent;
  let fixture: ComponentFixture<RecentallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
