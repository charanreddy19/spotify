import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SongComponent } from './song/song.component';
import { NavComponent } from './nav/nav.component';
import { BodyComponent } from './body/body.component';
import { SongsService } from './songs.service';
import { LoginComponent } from './login/login.component';
import { AllsongsComponent } from './allsongs/allsongs.component';
import { RecentallComponent } from './recentall/recentall.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { PlaylistsongsComponent } from './playlistsongs/playlistsongs.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidebarComponent,
    SongComponent,
    NavComponent,
    BodyComponent,
    LoginComponent,
    AllsongsComponent,
    RecentallComponent,
    RecommendedComponent,
    PlaylistComponent,
    PlaylistsongsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [SongsService,BodyComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
