import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import {SongsService} from '../songs.service'
import {Router,ActivatedRoute,ParamMap} from '@angular/router'

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  songs:any=[]
  madeForYou:any=[]
  playlists:any=[]
  recommended:any=[]
  recent:any=[]
  subscription:any
  song:any
  constructor(private songsService:SongsService,private router:Router,private route:ActivatedRoute) { 
    this.songsService.song.subscribe((value:any)=>{
      this.song=value
    })
    this.songsService.playlist.subscribe((value:any)=>{
      this.playlists=value
    })
  }

  ngOnInit(): void {
    this.songsService.fiveSongs().then((data:any)=>{
      this.songs=data
    })

    this.songsService.fiveRecent().then((data:any)=>{
      console.log(data)
      this.recent=data
    })

    this.songsService.playlists().then((data:any)=>{
      this.playlists=data
    })

    this.songsService.recommended().then((data:any)=>{
      this.recommended=data
    })
   
  }

  play(song:any) {
    this.songsService.song.next(song)
  }

  navigateToAll(event:any) {
    if(event.target.id=="fiveRecent"){
      this.router.navigate(['recent',{path:event.target.id}],{relativeTo:this.route})
    }
    else if(event.target.id=="fiveSongs"){
      this.router.navigate(['all',{path:event.target.id}],{relativeTo:this.route})
    }
    else if(event.target.id=="recommended") {
      this.router.navigate(['recommended',{path:event.target.id}],{relativeTo:this.route})
    }
    else if(event.target.id=="playlist") {
      this.router.navigate(['playlists',{path:event.target.id}],{relativeTo:this.route})
    }
  }
}