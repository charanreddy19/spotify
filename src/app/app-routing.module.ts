import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllsongsComponent } from './allsongs/allsongs.component';
import { BodyComponent } from './body/body.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { PlaylistsongsComponent } from './playlistsongs/playlistsongs.component';
import { RecentallComponent } from './recentall/recentall.component';
import { RecommendedComponent } from './recommended/recommended.component';

const routes: Routes = [
  {path:'',redirectTo:"login",pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {path:'home',
  component:HomeComponent,
  children:[
    {path:'',redirectTo:'index',pathMatch:'full'},
    {path:'index',component:BodyComponent},
    {path:'index/all',component:AllsongsComponent},
    {path:'index/recent',component:RecentallComponent},
    {path:'index/recommended',component:RecommendedComponent},
    {path:'index/playlists',component:PlaylistComponent},
    {path:'playlist',component:PlaylistComponent},
    {path:'playlistsongs',component:PlaylistsongsComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
