import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SongsService {

  constructor() { }
  imported:any=[]
  song:any=new Subject()
  paths:any=[]
  playlist:any=new Subject()
  playlistName:any=new Subject()
  allSongs():any{
    return new Promise((resolve,reject)=>{
        fetch('http://localhost:8000/songs')
        .then(async (res:any)=>{
          let data=await res.json()
          resolve(data)
        })
      })
  }

  fiveRecent():any {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/firstFiveRecent')
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        resolve(data)
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  recent():any {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/recentPlayed')
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        resolve(data)
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  fiveSongs():any {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/firstFive')
      .then(async (res:any)=>{
        let data=await res.json()
        console.log(data)
        resolve(data)
      })
    })
  } 

  songs():any {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/songs')
      .then(async (res:any)=>{
        let data=await res.json()
        console.log(data)
        resolve(data)
      })
    })
  } 




  getSongWithId(id:any) {
      return new Promise((resolve,reject)=>{
         fetch(`http://localhost:8000/${id}`)
         .then(async (res:any)=>{
           let data=await res.json()
           resolve(data)
         })
      })
  }

  playlists() {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/playlists`)
      .then(async (res:any)=>{
        let data=await res.json()
        resolve(data)
      })
   })
  }

  allPlaylists() {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/allplaylists`)
      .then(async (res:any)=>{
        let data=await res.json()
        resolve(data)
      })
   })
  }

  recommended() {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/recommended')
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        resolve(data)
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  allRecommended() {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/allrecommended')
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        resolve(data)
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  last() {
    return new Promise((resolve,reject)=>{
      fetch('http://localhost:8000/recent')
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
            fetch(`http://localhost:8000/${data[0]['song_id']}`)
            .then(async (response)=>{
              let data1=await response.json()
              resolve(data1[0])
            })
      })
    })
  }

  createPlaylist(name:any) {
     return new Promise((resolve,reject)=>{
       fetch(`http://localhost:8000/playlist?name=${name}`,{
         method:"POST"
       })
       .then((res)=>{
         resolve(res.json())
       })
     })
  }

  deletePlaylist(playlist:any) {
    return new Promise((resolve,reject)=>{
      let name=playlist.name
      let arr=name.split('_')
      fetch(`http://localhost:8000/deletePlaylist?name=${arr[0]}`,{
      method:"DELETE"
      })
      .then((res)=>{
        resolve(res.json())
      })
    })
  }

  removePlaylist(playlist:any) {
    return new Promise((resolve,reject)=>{
      let name=playlist.name
      let arr=name.split('_')
      fetch(`http://localhost:8000/removePlaylist?name=${arr[0]}`,{
      method:"PUT"
      })
      .then((res)=>{
        resolve(res.json())
      })
    })
  }

  insertPlaylist(name:any) {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/insertPlaylist?name=${name}`,{
        method:"POST"
      })
      .then((res)=>{
        resolve(res.json())
      })
    })
  }

  playlistSongs(name:any) {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/playlistsongs?name=${name}`)
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        resolve(data)
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  toAddPlaylistSongs(name:any) {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/playlistsongs?name=${name}`)
      .then(async (res:any)=>{
        let data=await res.json()
        let obj:any=[]
        for(let song of data) {
            fetch(`http://localhost:8000/${song.id}`)
            .then(async (response)=>{
              let data1=await response.json()
              obj.push(data1[0])
            })
        }
        return obj
      })
      .then((data)=>{
        fetch('http://localhost:8000/songs')
        .then(async (res:any)=>{
          let data1=await res.json()
          console.log(data1)
          let index
          for(let song of data){
            index=data1.indexOf(song)
            data1.splice(index,1)
          }
          resolve(data1)
        })
      })
      .catch((err)=>{
        reject(err)
      })
    })
  }

  addSongToPlaylist(id:any,name:any) {
    return new Promise((resolve,reject)=>{
      fetch(`http://localhost:8000/addsongtoplaylist?id=${id}&name=${name}`,{
        method:"PUT"
      })
      .then((res)=>{
        resolve(res.json())
      })
    })
  }
}