import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, ParamMap, Router } from '@angular/router';
import { SongsService } from '../songs.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  paths:any=['./']
  index:any=0

  constructor(private router:Router,private song:SongsService,private route:ActivatedRoute) { 

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd ) {
        console.log(event.url)
        this.paths.push(event.url)
        this.index=this.index+1
      }
    })
  }

  
  navigateNext() {
     this.index=this.index+1
     console.log(this.paths)
     if(this.paths[this.index]){
      this.router.navigate([this.paths[this.index]])
     }
  }

  navigateBack() {
    this.index=this.index-1
    console.log(this.paths)
    if(this.paths[this.index]){
     this.router.navigate([this.paths[this.index]])
    } 
  }

  navigate() {
    this.router.navigate(['/home'])
  }
  
  ngOnInit(): void {

  }





}