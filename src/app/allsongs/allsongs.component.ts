import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap } from '@angular/router';
import { SongsService } from '../songs.service';

@Component({
  selector: 'app-allsongs',
  templateUrl: './allsongs.component.html',
  styleUrls: ['./allsongs.component.css']
})
export class AllsongsComponent implements OnInit {
  
  songs:any=[]
  obj:any
  constructor(private song:SongsService,private route:ActivatedRoute) {
    this.song.song.subscribe((value:any)=>{
      this.obj=value
    })
   }

  ngOnInit(): void {
          this.song.songs().then((data:any)=>{
             this.songs=data
          })
  }

  play(song:any) {
    this.song.song.next(song)
  }
}