import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {SongsService} from '../songs.service'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  playlists:any=[]
  name:any="hello"
  input:any=false
  nameExists:any=false
  playlistName:any
  constructor(private songsService:SongsService,private router:Router) {
    this.songsService.playlist.subscribe((value:any)=>{
      this.playlists=value
    })
    this.songsService.playlistName.subscribe((value:any)=>{
      this.playlistName=value
    })
   }

  ngOnInit(): void {
    this.songsService.allPlaylists().then((data:any)=>{
      this.playlists=data
    })
  }

  show() {
    this.input=!this.input
   // let data= 
  }

  createPlaylist(text:any) {
    let insert=true;
    for(let song of this.playlists){
      if(song.name===text.value){
        insert=false
      }
    }
    if(insert){
      this.input=!this.input
      this.songsService.createPlaylist(text.value).then(console.log)
      this.songsService.insertPlaylist(text.value).then(console.log)
      this.playlists.push({name:text.value})
      this.songsService.playlist.next(this.playlists)
    }
    else {
       this.nameExists=true
       text.value=""
       text.placeholder="name already exists"
    }
  }

  navigate(event:any) {
    if(event.target.id=="home"){
     this.router.navigate(['/home'])
    }
    else if(event.target.id="playlist"){
      this.router.navigate(['/home/playlist'])
    }
    else if (event.target.id="library"){
      this.router.navigate(['/home'])
    }
  }

  navigateToPlaylist() {
    this.router.navigate(['/home/playlist'])
  }

  navigateToPlaylistSongs(playlist:any) {
    this.router.navigate(['/home/playlistsongs',{name:playlist.name}])
    this.songsService.playlistName.next(playlist.name)
  }

  deletePlaylist(playlist:any) {
     let index=this.playlists.indexOf(playlist)
     this.playlists.splice(index,1)
     this.songsService.playlist.next(this.playlists)
     this.songsService.deletePlaylist(playlist).then((data)=>{
       console.log(data)
     }).then(()=>{
       this.songsService.removePlaylist(playlist).then((data)=>{
         console.log(data)
       })
     })
  }
}