import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap ,Router} from '@angular/router';
import { SongsService } from '../songs.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  songs:any=[]
  obj:any
  
  constructor(private song:SongsService,private route:ActivatedRoute,private router:Router) { 
    this.song.song.subscribe((value:any)=>{
      this.obj=value
    })
  }

  ngOnInit(): void {
    this.song.allPlaylists().then((data:any)=>{
      this.songs=data
   })
  }

  navigate(song:any) {
    this.router.navigate(['/home/playlistsongs',{name:song.name}])
  }
   
  play(song:any) {
   // this.song.song.next(song)
  }

}
