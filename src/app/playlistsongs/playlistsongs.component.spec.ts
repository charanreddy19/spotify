import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsongsComponent } from './playlistsongs.component';

describe('PlaylistsongsComponent', () => {
  let component: PlaylistsongsComponent;
  let fixture: ComponentFixture<PlaylistsongsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlaylistsongsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsongsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
